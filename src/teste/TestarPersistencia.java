/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistence.daos.TasksJpaController;
import persistence.daos.TeamJpaController;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Tasks;
import persistence.entities.Team;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class TestarPersistencia {
    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";
    public static void main(String[] args) throws Exception {
        //criarUsuario("dap19953", "Daniel3" , "1233212");
        //listarUsuario();
        
        //List<Usuario> users = findAllUsuario();
        //users.add(user);
        //criarTeam("devs2", users);
        //System.out.println("Terminou");
        //Team team = findTeam(2L);
        //criarTask("Comer", "Se alimentar de pizza", findUsuario(1L), team);
        System.out.println("Terminou");
    }
    
    private static void criarUsuario(String username, String name, String password){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        Usuario user = new Usuario();
        user.setName(name);
        user.setUsername(username);
        user.setPassword(password);
        dao.create(user);
    }
    
    private static void criarTeam(String name, List<Usuario> users){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        TeamJpaController dao = new TeamJpaController(factory);
        Team team = new Team();
        team.setName(name);
        team.setUsuarios(users);
        dao.create(team);
    }
    
    public static void criarTask(String name, String descricao, Usuario creator, Team team){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        TasksJpaController dao = new TasksJpaController(factory);
        Tasks task = new Tasks();
        task.setName(name);
        task.setDescription(name);
        task.setCreator(creator);        
        task.setTeam(team);
        dao.create(task);
    }
    
    private static void listarUsuario(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        List<Usuario> lst = dao.findUsuarioEntities();
        for (Usuario user : lst){
            System.out.println(user);
        }
    }
    
    private static List<Usuario> findAllUsuario(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        return dao.findUsuarioEntities();
    }
    
    private static Usuario findUsuario(Long id){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        Usuario user = dao.findUsuario(id);
        System.out.println(user);
        return user;
    }
    
    private static void editTeam(Team team) throws Exception{
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        TeamJpaController dao = new TeamJpaController(factory);
        dao.edit(team);
        System.out.println(team);
    }
    
    private static Team findTeam(Long id){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        TeamJpaController dao = new TeamJpaController(factory);
        Team team = dao.findTeam(id);
        System.out.println(team.getName());
        return team;
    }
    
    private static Usuario Autenticar(String username, String password){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        return dao.findUsuario(username, password);
    }
}
