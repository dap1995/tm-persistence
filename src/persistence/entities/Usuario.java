/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.eclipse.persistence.annotations.CascadeOnDelete;

/**
 *
 * @author dap
 */
@Entity
@NamedQueries({
 @NamedQuery(name = "Usuario.authenticate", query = "SELECT o FROM Usuario o WHERE o.username= :username AND o.password= :password"),
 @NamedQuery(name = "Usuario.findByUsername", query = "SELECT o FROM Usuario o WHERE o.username = :username")
})
@CascadeOnDelete
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usuarioId")
    private Long id;
    
    @Column(unique = true, nullable = false, length = 45)
    private String username;
    
    @Column(unique = false, nullable = false, length = 45)
    private String name;
    
    @Column(unique = false, nullable = false, length = 45)
    private String password;
    
    @ManyToMany(mappedBy = "usuarios")
    private List<Team> team;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Team> getTeam() {
        return team;
    }

    public void setTeam(List<Team> team) {
        this.team = team;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " [ id=" + id + " | name=" + name +  " | email=" +  username +  " | senha=" +  password + " ]";

    }
    
}
